Source: python-multipletau
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alexandre Mestiashvili <mestia@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-numpy,
               python3-numpydoc,
               python3-pytest,
               python3-pytest-runner,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx,
               python3-sphinx-rtd-theme,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-multipletau
Vcs-Git: https://salsa.debian.org/python-team/packages/python-multipletau.git
Homepage: https://github.com/FCS-analysis/multipletau
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python3-multipletau
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: multiple-tau algorithm for Python3/NumPy
 Multiple-tau correlation is computed on a logarithmic scale (less
 data points are computed) and is thus much faster than conventional
 correlation on a linear scale such as `numpy.correlate`
 .
 An online reference is available
 at http://paulmueller.github.io/multipletau

Package: python-multipletau-doc
Architecture: all
Section: doc
Depends: libjs-mathjax,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: documentation for multipletau Python module
 This package contains HTML documentation for python-multipletau
 .
 An online reference is available
 at http://paulmueller.github.io/multipletau
